<?php
namespace Easy\Core;

use Easy\Core\Exceptions\HttpException;
use Easy\Core\Exceptions\InvalidCallException;
use Easy\Core\Exceptions\InvalidConfigException;

defined('EASY_PATH') or define('EASY_PATH', __DIR__);

class Application
{
    /**
     * @var \Easy\Core\Application application instance
     */
    public static $app;

    public $appName = 'defaultApp';

    /**
     * @var array
     */
    protected $config;

    /**
     * @var string
     */
    public $currentController;

    /**
     * @var string
     */
    public $currentAction;

    /**
     * @var array
     */
    private $params = array();

    public $defaultController;
    public $defaultAction;
    private $appRouter;

    /**
     * Application constructor
     * @param array $config
     */
    public function __construct(array $config) {
        static::$app = $this;

        $this->configure($config);
        $this->config = $config;
    }

    /**
     * Run configured application
     */
    public function run() {
        $query = Request::get('q');
        if(is_null($query) || $query == '') {
            $response = $this->runAction($this->defaultController, $this->defaultAction);
        } else {
            $this->appRouter->processRequest($query);
            $controller = $this->appRouter->getController() . 'Controller';
            $response = $this->runAction($controller, $this->appRouter->getAction());
        }

        $this->sendResponse($response);
    }

    public function sendResponse($response) {
        echo $response;
    }

    public function runAction($controller, $action, $params = null) {
        $controllerClass = '\Easy\Controllers\\' . $controller;
        if(!class_exists($controllerClass)) {
            throw new InvalidCallException("Controller '{$controllerClass}' not found");
        }

        $this->currentController = $controller;
        $controller = new $controllerClass($this->config);

        if(is_null($action) || $action == '') {
            $action = 'index';
        }

        $this->currentAction = $action;
        $actionMethod = 'action' . ucfirst($action);

        if(method_exists($controller, $actionMethod) && is_callable([$controller, $actionMethod])) {
            try {
                if(is_null($params)) {
                    return $controller->$actionMethod();
                } else {
                    return $controller->$actionMethod($params);
                }
            } catch(HttpException $e) {
                return $this->runErrorAction($e);
            }
        } else {
            throw new InvalidCallException("Action '{$actionMethod}' not found");
        }
    }

    public function runErrorAction($e) {
        $errorController = $this->config['errorController'];

        return $this->runAction($errorController, 'error', $e);
    }

    private function configure(array $config) {
        if(is_null($config) || !is_array($config)) {
            throw new InvalidConfigException("Config is not exists or jnvalid");
        }
        if(!isset($config['defaultController'])) {
            throw new InvalidConfigException("'defaultController' property is required");
        }
        if(!isset($config['defaultAction'])) {
            throw new InvalidConfigException("'defaultAction' property is required");
        }
        if(!isset($config['router']) || !isset($config['router']['class'])) {
            throw new InvalidConfigException("Router must be configured");
        }

        $routerClass = $config['router']['class'];
        if(!class_exists($routerClass)) {
            throw new InvalidConfigException("Router class '{$routerClass}'' not found");
        }
        $router = new $routerClass();
        if(!($router instanceof RouterInterface)) {
            throw new InvalidConfigException("Router class '{$routerClass}' must implements 'RouterInteface'");
        }

        $this->appRouter = $router;

        if(isset($config['defaultController'])) $this->defaultController = $config['defaultController'];
        if(isset($config['defaultAction'])) $this->defaultAction = $config['defaultAction'];
        if(isset($config['appName'])) $this->appName = $config['appName'];

    }

    public function createAuthSession($identity) {
        $_SESSION['easy_sessions']['auth'] = $identity;
    }

    public function destroyAuthSession() {
        if(isset($_SESSION['easy_sessions']['auth'])) {
            unset($_SESSION['easy_sessions']['auth']);
        }
    }

    public function getCurrentSession() {
        if(isset($_SESSION['easy_sessions']['auth'])) {
            return $_SESSION['easy_sessions']['auth'];
        }

        return false;
    }

    public function isGuest() {
        return $this->getCurrentSession() === false;
    }

    public function getConfig($entry = null) {
        if(is_null($entry)) {
            return $this->config;
        }
        if(!isset($this->config[$entry])) {
            throw new InvalidCallException("Config entry '{$entry}' not found");
        }

        return $this->config[$entry];
    }
}