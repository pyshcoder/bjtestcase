<?php
namespace Easy\Core;

class Request {

    /**
     * Return $_GET param by key
     * @param string $key
     * @return mixed
     */
    public static function get($key) {
        return isset($_GET[$key]) ? $_GET[$key] : null;
    }

    /**
     * Return $_POST param by key
     * @param string $key
     * @return mixed
     */
    public static function post($key) {
        return isset($_POST[$key]) ? $_POST[$key] : null;
    }

}