<?php
namespace Easy\Helpers;

use Easy\Core\Application;

class ViewHelper {
    public static function activeClass($controller, $cssClass = 'active') {
        $controller = ucfirst($controller) . 'Controller';
        if($controller == Application::$app->currentController) {
            return $cssClass;
        }

        return '';
    }

    public static function linkTo($controller, $method = '', $params = []) {
        $link = "/{$controller}";
        if($method != '') {
            $link .= "/{$method}";
        }
        if(!is_null($params) && is_array($params) && count($params) > 0) {
            $link .= '?' . http_build_query($params);
        }

        return $link;
    }
}