<?php
namespace Easy\Controllers;

use Easy\Core\Application;
use Easy\Core\Controller;
use Easy\Core\Exceptions\HttpException;
use Easy\Core\Request;
use Easy\Models\Task;

class TaskController extends Controller {
    private $tasksPerPage = 3;

    public function actionIndex() {
        $tasks = Task::limit($this->tasksPerPage, 0);

        $taskCount = Task::count();
        $pagination = [
            'currentPage' => 1,
            'totalPages' => ceil($taskCount / $this->tasksPerPage),
        ];
        $page = Request::get('page');
        if($page !== null) {
            $offset = ($page - 1) * $this->tasksPerPage;
            $tasks = Task::limit($this->tasksPerPage, $offset);
            $pagination['currentPage'] = $page;
        }

        return $this->render('index', [
            'tasks' => $tasks,
            'pagination' => $pagination,
        ]);
    }

    public function actionCreate() {
        $task = Request::post('task');
        if($task !== null) {
            $task = array_map(function($element) {
                return filter_var($element, FILTER_SANITIZE_STRING);
            }, $task);

            $taskModel = new Task();
            $taskModel->username = $task['username'];
            $taskModel->email = $task['email'];
            $taskModel->title = $task['title'];
            $taskModel->text = $task['text'];
            if(isset($task['completed']) && $task['completed'] == 'on') {
                $taskModel->completed = true;
            } else {
                $taskModel->completed = false;
            }
            $taskModel->save();

            return $this->redirect('task');
        }


        $emptyTask = new Task();
        return $this->render('create', [
            'task' => $emptyTask,
        ]);
    }

    public function actionUpdate() {
        if(Application::$app->isGuest()) {
            throw new HttpException(403, 'Forbidden');
        }

        $task = Task::one(Request::get('id'));
        if($task === false) {
            throw new HttpException(404, 'Task not found');
        }

        $postTask = Request::post('task');
        if($postTask !== null) {
            $postTask = array_map(function($element) {
                return filter_var($element, FILTER_SANITIZE_STRING);
            }, $postTask);

            $taskModel = new Task();
            $taskModel->id = $task->id;
            $taskModel->username = $postTask['username'];
            $taskModel->email = $postTask['email'];
            $taskModel->title = $postTask['title'];
            $taskModel->text = $postTask['text'];

            if(isset($postTask['completed']) && $postTask['completed'] == 'on') {
                $taskModel->completed = true;
            } else {
                $taskModel->completed = false;
            }
            $taskModel->update();

            return $this->redirect('task');
        }

        return $this->render('update', [
            'task' => $task,
        ]);
    }
}