<?php
namespace Easy\Controllers;

use Easy\Core\Controller;

class ErrorController extends Controller {
    public function actionError($exception) {
        return $this->render('error', [
            'exception' => $exception,
        ]);
    }
}