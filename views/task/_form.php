<div class="row">
    <form method="POST" class="form-horizontal">
        <div class="col-md-6">
            <div class="form-group">
                <label for="inputUsername" class="col-sm-2 control-label">Username</label>
                <div class="col-sm-10">
                    <input name="task[username]" class="form-control" id="inputUsername" placeholder="Username" value="<?= $task->username ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input name="task[email]" type="email" class="form-control" id="inputEmail" placeholder="Email" value="<?= $task->email ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="inputTitle" class="col-sm-2 control-label">Task title</label>
                <div class="col-sm-10">
                    <input name="task[title]" class="form-control" id="inputTitle" placeholder="Title" value="<?= $task->title ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="inputText" class="col-sm-2 control-label">Task text</label>
                <div class="col-sm-10">
                    <textarea name="task[text]" class="form-control" rows="3" id="inputText">
                        <?= $task->text ?>
                    </textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input name="task[completed]" type="checkbox" <?= ((int)$task->completed === 1) ? 'checked' : '' ?>> Task completed
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default"><?= ($task->id == null) ? 'Create' : 'Update' ?></button>
                </div>
            </div>
        </div>
    </form>
</div>
