<?php
use Easy\Helpers\ViewHelper;
use Easy\Core\Application;
?>

<div class="row">
    <div class="col-sm-3" style="background-color: #f1f1f1; height: 100%;">
        <a href="<?= ViewHelper::linkTo('task', 'create') ?>" class="btn btn-primary btn-lg center-block" style="margin-top: 15px;">Create task</a>
    </div>
    <div class="col-sm-9">
        <?php if(count($tasks) == 0): ?>
            <div class="alert alert-success" role="alert">
                No tasks found. Create a first one.
            </div>
        <?php else: ?>
            <?php foreach($tasks as $task): ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-sm-10">
                                <?php if((int)$task->completed == 1): ?>
                                    <h3 class="panel-title"><s><?= $task->title ?></s></h3>
                                <?php else: ?>
                                    <h3 class="panel-title"><?= $task->title ?></h3>
                                <?php endif; ?>
                            </div>
                            <?php if(!Application::$app->isGuest()): ?>
                                <div class="col-sm-1">
                                    <a href="<?= ViewHelper::linkTo('task', 'update', ['id' => $task->id]) ?>" class="btn btn-success btn-xs">Edit task</a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="panel-body">
                        <?= $task->text ?>
                    </div>
                    <div class="panel-footer">
                        <span>Created by <b><?= $task->username ?></b> (<?= $task->email ?>)</span>
                    </div>
                </div>
            <?php endforeach; ?>
            <?php if($pagination['totalPages'] > 1): ?>
                <nav>
                    <ul class="pagination">
                        <?php if($pagination['currentPage'] == 1): ?>
                            <li class="disabled"><a href="#"><span>&laquo;</span>
                        <?php else: ?>
                            <li>
                                <a href="<?= ViewHelper::linkTo('task', 'index', ['page' => ($pagination['currentPage'] - 1)]) ?>">
                                    <span>&laquo;</span>
                                </a>
                            </li>
                        <?php endif; ?>

                        <?php for($page = 1; $page < $pagination['totalPages'] + 1; $page++): ?>
                            <?php if($page == $pagination['currentPage']): ?>
                                <li class="active"><a href="#"><?= $page ?></a></li>
                            <?php else: ?>
                                <li>
                                    <a href="<?= ViewHelper::linkTo('task', 'index', ['page' => $page]) ?>"><?= $page ?></a>
                                </li>
                            <?php endif; ?>
                        <?php endfor; ?>

                        <?php if($pagination['currentPage'] == $pagination['totalPages']): ?>
                            <li class="disabled"><a href="#"><span>&raquo;</span>
                        <?php else: ?>
                            <li>
                                <a href="<?= ViewHelper::linkTo('task', 'index', ['page' => ($pagination['currentPage'] + 1)]) ?>">
                                    <span>&raquo;</span>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </nav>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>