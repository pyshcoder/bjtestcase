<?php
use Easy\Helpers\ViewHelper;
?>

<div class="row">
    <div class="col-md-12">
        <div style="padding: 40px 15px; text-align: center;">
            <h1>Oops!</h1>
            <h2><?= $exception->getName() ?></h2>
            <div class="error-details">
                <?= $exception->getMessage() ?>
            </div>
            <div style="margin-top:15px; margin-bottom:15px;">
                <a href="<?= ViewHelper::linkTo('application') ?>" class="btn btn-primary btn-lg">
                    <span class="glyphicon glyphicon-home"></span> Go home
                </a>
            </div>
        </div>
    </div>
</div>