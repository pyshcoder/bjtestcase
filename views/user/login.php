<div class="row">
    <?php if(count($errors > 0)): ?>
        <?php foreach($errors as $error): ?>
            <div class="alert alert-danger" role="alert">
                <?= $error ?>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>

    <form method="POST">
        <div class="col-md-6">
            <div class="form-group">
                <label for="usernameInput">Login</label>
                <input class="form-control" id="usernameInput" placeholder="Login" name="username">
            </div>
            <div class="form-group">
                <label for="passwordInput">Password</label>
                <input type="password" class="form-control" id="passwordInput" placeholder="Password" name="password">
            </div>
            <button type="submit" class="btn btn-success">Login</button>
        </div>
    </form>
</div>
