<?php
use \Easy\Core\Application;
use Easy\Helpers\ViewHelper;
?>

<html lang="ru">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?= $this->title ?></title>

    <?= $this->renderCssIncludes() ?>
</head>

<body>

<div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?= ViewHelper::linkTo('application') ?>"><?= Application::$app->appName ?></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="<?= ViewHelper::activeClass('application') ?>">
                        <a href="<?= ViewHelper::linkTo('application')?>">Home</a>
                    </li>
                    <li class="<?= ViewHelper::activeClass('task') ?>">
                        <a href="<?= ViewHelper::linkTo('task')?>">Tasks</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <?php if(Application::$app->isGuest()): ?>
                        <li><a href="<?= ViewHelper::linkTo('user', 'login')?>">Login</a></li>
                    <?php else: ?>
                        <li><a href="<?= ViewHelper::linkTo('user', 'logout')?>">Logout (<?= Application::$app->getCurrentSession()->username ?>)</a></li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </nav>

    <?= $content ?>

</div>


<?= $this->renderJsIncludes() ?>
</body>

</html>