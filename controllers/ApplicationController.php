<?php
namespace Easy\Controllers;

use Easy\Core\Controller;

class ApplicationController extends Controller {
    public function actionIndex() {
        return $this->render('index');
    }
}