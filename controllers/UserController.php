<?php
namespace Easy\Controllers;

use Easy\Core\Application;
use Easy\Core\Controller;
use Easy\Core\Request;
use Easy\Models\User;
use Easy\Core\Exceptions\HttpException;

class UserController extends Controller {
    public function actionLogin() {
        if(!Application::$app->isGuest()) {
            return $this->redirect('application');
        }
        $errors = [];
        $username = Request::post('username');
        if($username !== null) {
            $password = Request::post('password');
            $user = new User([
                'username' => $username,
                'password' => $password,
            ]);
            if($user->checkCredentials()) {
                Application::$app->createAuthSession($user);
                return $this->redirect('application');
            } else {
                $errors[] = 'Login or password incorrect';
            }
        }

        return $this->render('login', [
            'errors' => $errors,
        ]);
    }

    public function actionLogout() {
        if(Application::$app->isGuest()) {
            throw new HttpException(403, "You're are not logged in");
        }
        Application::$app->destroyAuthSession();

        return $this->redirect('application');
    }
}