<?php
namespace Easy\Core;

interface RouterInterface {
    public function processRequest($requestString);
    public function getController();
    public function getAction();
}