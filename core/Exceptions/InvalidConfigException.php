<?php
namespace Easy\Core\Exceptions;

class InvalidConfigException extends Exception {
    public function getName() {
        return 'Invalid Config Exception';
    }
}