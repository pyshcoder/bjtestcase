<?php
namespace Easy\Core\Exceptions;

class Exception extends \Exception {
    public function getName() {
        return 'Exception';
    }
}