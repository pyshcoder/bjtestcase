<?php
namespace Easy\Core\Exceptions;

class InvalidCallException extends Exception {
    public function getName() {
        return 'Invalid Call Exception';
    }
}