<?php
namespace Easy\Core;

use Easy\Core\Exceptions\InvalidConfigException;

class Model {
    protected $config;

    public $db;

    public function __construct() {
        $this->config = Application::$app->getConfig();
        $this->db = Database::connection();
    }
}