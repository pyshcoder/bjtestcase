<?php
namespace Easy\Core\Drivers;

interface DriverInterface {
    public function connect();


    /**
     * @return \PDO
     */
    public function getConnection();
}