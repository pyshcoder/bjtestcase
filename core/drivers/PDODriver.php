<?php
namespace Easy\Core\Drivers;

use Easy\Core\Drivers\DriverInterface;

class PDODriver implements DriverInterface {
    private $connection;
    private $config;

    public function __construct(array $config) {
        $this->config = $config;
        $this->connect();
    }

    public function connect() {
        $this->connection = new \PDO(
            $this->config['dsn'],
            $this->config['username'],
            $this->config['password'],
            $this->config['options']
        );
    }


    /**
     * @return \PDO
     */
    public function getConnection() {
        return $this->connection;
    }
}