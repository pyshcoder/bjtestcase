<?php
namespace Easy\Core;

use Easy\Core\Exceptions\InvalidCallException;

class Controller {
    public $layout = 'main';
    public $baseViewPath = 'views/';
    public $viewPath;
    public $title;

    public function __construct() {
        $this->config = Application::$app->getConfig();

        $this->title = str_replace('Controller', '', (new \ReflectionClass($this))->getShortName());

        if(isset($config['controller'])) {
            $this->configure($config['controller']);
        }
    }

    private function configure(array $config) {
        if(isset($config['defaultLayout'])) $this->layout = $config['defaultLayout'];
        if(isset($config['baseViewPath'])) $this->baseViewPath = $config['baseViewPath'];
    }

    public function redirect($controller, $action = null) {
        $link = "/{$controller}";
        if(!is_null($action)) {
            $link .= "/{$method}";
        }

        header('Location: ' . $link);

        return null;
    }

    public function findViewFile($view) {
        if(is_null($this->viewPath)) {
            $controllerName = strtolower((new \ReflectionClass($this))->getShortName());
            $controllerName = str_replace('controller', '', $controllerName);
            $viewDir = $controllerName . '/';
        } else {
            $viewDir = $this->viewPath;
        }

        return APP_PATH . $this->baseViewPath . $viewDir . $view . '.php';
    }

    public function findLayoutFile($layout) {
        return APP_PATH . $this->baseViewPath . 'layouts/' . $layout . '.php';
    }

    public function render($view, array $data = []) {
        $viewFile = $this->findViewFile($view);
        $content = $this->renderFile($viewFile, $data);

        return $this->renderContent($content);
    }

    public function renderPartial($view, array $data = []) {
        $viewFile = $this->findViewFile($view);
        return $this->renderFile($viewFile, $data);
    }

    public function renderContent($content) {
        if(!is_null($this->layout) || $this->layout == '') {
            $layoutFile = $this->findLayoutFile($this->layout);
            return $this->renderFile($layoutFile, ['content' => $content]);
        }

        return $content;
    }

    public function renderFile($file, array $data = []) {
        if(!file_exists($file)) {
            throw new InvalidCallException("View file '{$file}' not found");
        }

        extract($data, EXTR_OVERWRITE);

        ob_start();
        ob_implicit_flush(false);

        require($file);

        return ob_get_clean();
    }

    public function getAssets() {
        return Application::$app->getConfig('assets');
    }

    public function renderCssIncludes() {
        $assets = $this->getAssets();
        if(!isset($assets[$this->layout])) {
            return; // No assets for this layout, do nothing.
        }
        if(!isset($assets[$this->layout]['css'])) {
            return; // No CSS assets for this layout, do nothing.
        }
        $cssFiles = $assets[$this->layout]['css'];
        if(count($cssFiles) > 0) {
            foreach($cssFiles as $file) {
                return "<link rel=\"stylesheet\" href=\"{$file}\">";
            }
        }
    }

    public function renderJsIncludes() {
        $assets = $this->getAssets();
        if(!isset($assets[$this->layout])) {
            return; // No assets for this layout, do nothing.
        }
        if(!isset($assets[$this->layout]['js'])) {
            return; // No JS assets for this layout, do nothing.
        }
        $jsFiles = $assets[$this->layout]['js'];
        if(count($jsFiles) > 0) {
            foreach($jsFiles as $file) {
                return "<script src=\"{$file}\"></script>";
            }
        }
    }
}