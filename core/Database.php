<?php
namespace Easy\Core;

use Easy\Core\Exceptions\InvalidCallException;

class Database {
    private static $_instance;


    /**
     * @return \Easy\Core\Drivers\PDODriver
     * @throws InvalidCallException
     */
    public static function getInstance() {
        if(is_null(static::$_instance)) {
            $driverClass = static::getDriverClass();
            if(!class_exists($driverClass)) {
                throw new InvalidCallException("Database driver class '{$driverClass}' not found");
            }
            $config = Application::$app->getConfig('database');
            static::$_instance = new $driverClass($config['connections'][$config['selectedConnection']]);
        }

        return static::$_instance;
    }

    public static function connection() {
        return static::getInstance()->getConnection();
    }

    private static function getDriverClass() {
        $config = Application::$app->getConfig('database');
        $connection = $config['connections'][$config['selectedConnection']];

        return $connection['driver'];
    }
}