<?php
namespace Easy\Core;

class DefaultRouter implements RouterInterface {

    private $controller;
    private $action;

    public function processRequest($requestString) {
        $requestParts = explode('/', filter_var(rtrim($requestString, '/'), FILTER_SANITIZE_URL));
        $this->controller = isset($requestParts[0]) ? ucfirst($requestParts[0]) : null;
        $this->action = isset($requestParts[1]) ? $requestParts[1] : null;
    }

    public function getController() {
        return $this->controller;
    }

    public function getAction() {
        return $this->action;
    }
}