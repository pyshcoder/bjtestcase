<?php

define('APP_PATH', __DIR__ . '/../');
define('APP_ENV', 'PROD');

require_once __DIR__ . '/../vendor/autoload.php';
$config = require_once __DIR__ . '/../config/main.php';

session_start();

(new Easy\Core\Application($config))->run();