<?php

return [
    'appName' => 'beeJEE',

    'errorController' => 'ErrorController',
    'defaultController' => 'ApplicationController',
    'defaultAction' => 'index',

    'router' => [
        'class' => '\Easy\Core\DefaultRouter',
    ],

    'controller' => [
        'defaultLayout' => 'main',
    ],

    'database' => [
        'selectedConnection' => 'production',

        'connections' => [
            'development' => [
                'driver' => '\Easy\Core\Drivers\PDODriver',
                'dsn' => 'mysql:host=127.0.0.1;port=8889;dbname=bjtestcase;charset=utf8mb4',
                'username' => 'root',
                'password' => 'qweqwe123',
                'options' => [
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING
                ],
            ],
            'production' => [
                'driver' => '\Easy\Core\Drivers\PDODriver',
                'dsn' => 'mysql:host=mysql.zzz.com.ua;port=3306;dbname=sgtfox;charset=utf8mb4',
                'username' => 'bjtestcase',
                'password' => 'Qweqwe123',
                'options' => [
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING
                ],
            ]
        ],
    ],

    'assets' => [
        'main' => [
            'js' => [
                'https://code.jquery.com/jquery-1.12.4.min.js',
            ],
            'css' => [
                'https://stackpath.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css',
            ],
        ]
    ]
];