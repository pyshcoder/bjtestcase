<?php
namespace Easy\Models;

use Easy\Core\Database;
use Easy\Core\Model;

class Task extends Model {
    public $id;
    public $username;
    public $email;
    public $title;
    public $text;
    public $completed;

    public function save() {
        $sql = 'INSERT INTO tasks (username, email, title, text, completed) VALUES (:username, :email, :title, :text, :completed)';
        $query = $this->db->prepare($sql);

        $query->execute([
            ':username' => $this->username,
            ':email' => $this->email,
            ':title' => $this->title,
            ':text' => $this->text,
            ':completed' => $this->completed,
        ]);
    }

    public function update() {
        $sql = 'UPDATE tasks SET username = :username, email = :email, title = :title, text = :text, completed = :completed WHERE id = :id';
        $query = $this->db->prepare($sql);

        //$query->bindValue(':id', (int) $this->id, \PDO::PARAM_INT);

        $query->execute([
            ':username' => $this->username,
            ':email' => $this->email,
            ':title' => $this->title,
            ':text' => $this->text,
            ':completed' => $this->completed,
            ':id' => $this->id,
        ]);
    }

    public static function all() {
        $db = Database::connection();

        $query = $db->prepare('SELECT * FROM tasks');
        $query->execute();

        return $query->fetchAll();
    }

    public static function one($id) {
        $db = Database::connection();

        $query = $db->prepare('SELECT * FROM tasks WHERE id = :id LIMIT 1');
        $query->bindValue(':id', (int) $id, \PDO::PARAM_INT);
        $query->execute();

        return $query->fetch();
    }

    public static function limit($limit, $offset) {
        $db = Database::connection();

        $query = $db->prepare('SELECT * FROM tasks ORDER BY id DESC LIMIT :limit OFFSET :offset');
        $query->bindValue(':limit', (int) $limit, \PDO::PARAM_INT);
        $query->bindValue(':offset', (int) $offset, \PDO::PARAM_INT);
        $query->execute();

        return $query->fetchAll();
    }

    public static function count() {
        $db = Database::connection();

        $query = $db->prepare('SELECT COUNT(id) AS amount FROM tasks');
        $query->execute();

        return $query->fetch()->amount;
    }
}