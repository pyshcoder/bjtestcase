<?php
namespace Easy\Models;

use Easy\Core\Model;

class User extends Model {
    private $dataStorage = [
        'username' => 'admin',
        'password' => '123',
    ];

    public $username;
    public $password;

    public function __construct($configArray) {
        $this->username = $configArray['username'];
        $this->password = $configArray['password'];

        // We don't need connect to database in this class, so don't call parent constructor
    }

    public function checkCredentials() {
        if($this->username == $this->dataStorage['username'] && $this->password == $this->dataStorage['password']) {
            return true;
        }

        return false;
    }
}